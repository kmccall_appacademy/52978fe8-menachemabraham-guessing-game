# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  target = rand(1..100)
  count = 1

  puts "guess a number"
  loop do
    guess = gets.chomp.to_i
    guess == target ? break : puts(guess)
    guess > target ? puts(" too high") : puts(" too low")

    count += 1
  end

  puts "You got it in #{count} guesses"
  puts target
end

def shuffle_file(input_path)
  input_arr = File.readlines(input_path)

  output_path = input_path.sub(".", "-shuffled.")
  File.open(output_path, "w") do |f|
    input_arr.shuffle.each { |line| f.puts(line) }
  end
end

if $PROGRAM_NAME == __FILE__
  ARGV.empty? ? guessing_game : shuffle_file(ARGV[0].chomp)
end
